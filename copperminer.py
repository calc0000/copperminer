import os
import re
import sqlite3
import sys
import time
import urllib.request
from multiprocessing.pool import ThreadPool

from bs4 import BeautifulSoup

class Copperminer (object):
    def __init__ (self, baseUrl):
        if not baseUrl.endswith('/'): baseUrl = baseUrl + '/';
        self.base   = baseUrl;
        self.pool       = ThreadPool(20);   # since most of the thread's time will be spent on http, we can have a lot
        self.imgPool    = ThreadPool(20);
    def start (self, url = '/'):
        self.pool.apply_async(self.parse, [url, "root"]);
    def log (self, message):
        print("%s: %s" % (time.strftime("%Y/%m/%d %H:%M:%S", time.localtime()), message));
    def getUrl  (self, url):
        if url[0] == '/': url = url[1:];
        if not url.startswith(self.base):
            url = self.base + url;
        self.log("Fetching %s" % url);
        return urllib.request.urlopen(url);
    def download (self, url, dest):
        dirname = os.path.dirname(dest);
        if not os.path.exists(dirname):
            try:
                os.makedirs(dirname);
            except:
                pass;
        with self.getUrl(url) as pic:
            with open(dest, 'wb') as dump:
                dump.write(pic.read());
    def parse (self, url, name):
        try:
            with self.getUrl(url) as u:
                soup    = BeautifulSoup(u.read().decode('utf-8'));
                # look for categories
                for cat in soup.find_all("span", {"class": "catlink"}):
                    link    = cat.find_all("a")[0];
                    self.log("Firing Category %s (%s)" % (link['href'], link.string));
                    self.pool.apply_async(self.parse, [link['href'], link.string]);
                # look for albums
                for album in soup.find_all("span", {"class": "alblink"}):
                    link    = album.find_all("a")[0];
                    self.log("Firing Album %s (%s)" % (link['href'], link.string));
                    self.pool.apply_async(self.parse, [link['href'], link.string]);
                # look for pics
                for img in soup.find_all("a", href = re.compile(r'^displayimage\.php')):
                    img = img.find_all("img")[0];
                    img['src'] = img['src'].replace('thumb_', '');
                    self.log("Found pic %s" % img['src']);
                    if img['src'][0] != '/': img['src'] = '/' + img['src'];
                    dest = 'scraped' + img['src'];
                    self.imgPool.apply_async(self.download, [img['src'], dest]);
                # look for additional pages
                if 'page' not in url:
                    for script in soup.find_all("script", text = re.compile(r'.*Jump to page.*')):
                        # pull out page count
                        pageCount = int(re.search('page <= (\\d+)', script.string).group(1));
                        self.log("GOT %d PAGES" % pageCount);
                        for page in range(2, pageCount + 1):
                            self.log("Paging %s" % (url + '&page=%d' % page));
                            self.pool.apply_async(self.parse, [url + '&page=%d' % page, name]);
        except Exception as e:
            print(e);

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: %s <base url>" % sys.argv[0]);
    else:
        start = '/';
        if len(sys.argv) > 2:
            start = sys.argv[2];
        c   = Copperminer(sys.argv[1]);
        c.start(start);
        input();
